var glossaryControllers = angular.module('glossaryControllers',[]);

glossaryControllers.controller('ConceptsCtrl', ['$scope', '$sce', '$routeParams','$http', '$q','$location', '$anchorScroll','$filter','$timeout','ivhTreeviewMgr',
	function($scope,$sce,$routeParams,$http,$q,$location, $anchorScroll,$filter,$timeout,ivhTreeviewMgr) {


        /*
         * 
         * ===================================== IMPORTING DATA
         * 
         */

		// Pattern from http://stackoverflow.com/questions/33368495/wait-for-a-scope-variable-from-parent-controller-to-be-defined
        $scope.dataLoaded = false;

        // import of concept systems from file
        $scope.conceptSystem = {};
        $scope.conceptSystem.file = null;
        $scope.conceptSystem.format = "4";
        $scope.conceptSystem.resetBeforeLoad = true;
        $scope.conceptSystem.loading = false;
        $scope.conceptSystem.includeRid = true; //now used only for export
        $scope.categories = [];
        $scope.concepts = [];
        $scope.conceptTrees = [];
        //var rootConcepts = [];
        var myReader = new FileReader();
        $scope.loadConceptSystem = function () {
            $scope.conceptSystem.loading = true;
            switch ($scope.conceptSystem.format) {
                case "1":
                    myReader.onload = getOnLoadFunction(ibmXmlOnload);
                    break;
                /*case "2":
                    myReader.onload = getOnLoadFunction(m1WithoutRelationsJsonOnload);
                    break;*/
                case "3":
                    myReader.onload = getOnLoadFunction(m1WithRelationsXmlOnload);
                    break;
                case "4":
                    myReader.onload = getOnLoadFunction(glossaryToolJsonOnload);
                    break;
                default:
                    myReader.onload = getOnLoadFunction(glossaryToolJsonOnload);
            }
            myReader.readAsText($scope.conceptSystem.file);
        };
        
        // This function takes a function processorFunction and returns another function from that function.
        // The argument processorFunction(event) is expected to 
        // populate the global variables $scope.categories, ..., $scope.conceptTrees.
        // The argument event is really never used...
        // Instead the processorFunction is expected to read from the global variable myReader.result
        // The returned function clears these global variables, runs the processorFunction and then changes path.
        var getOnLoadFunction = function (processorFunction) {
            return function (event) {
                if ($scope.conceptSystem.resetBeforeLoad) {
                    $scope.categories = [];
                    $scope.concepts = [];
                    $scope.conceptTrees = [];
                }
                processorFunction(event);
                //validate that the concepts are unique
                /*var counts = [];
                for(var i = 0; i < $scope.concepts.length; i++) {
                    if(counts[$scope.concepts[i]._rid] === undefined) {
                        counts[$scope.concepts[i]._rid] = 1;
                    } else {
                        console.log("Duplicate!");
                    }
                }*/
                $scope.dataLoaded = true;
                $scope.conceptSystem.loading = false;
                //$scope.conceptSystemJSONUrl = getConceptSystemJSONUrl();
                //$scope.conceptSystemIGCXMLUrl = getConceptSystemIGCXMLUrl();
                $scope.conceptSystem.file = null;             // free memory
                $location.path("#").replace();
                if ($scope.conceptTrees.length == 0)
                    $scope.conceptTrees = null; // apply method gives error in case empty array
                $scope.$apply();
            };
        };

        // This function is passed as a processorFunction(event) to the getOnLoadFunction
        // for loading from IBM XML format
        // Event argument is never read from myReader.result which is expected to contain XML.
        // $scope.categories, $scope.concepts and $scope.conceptTrees are filled
        var m1WithRelationsXmlOnload = function (event) {
            //console.log(myReader.result);
            /* x2js is the factory for transforming XML to JSON
            the arrayAccessFormPaths parameter is used to tell the parser which
            positions in the XML tree may repeated more than once.
            Then, even if they are only occurring once they will anyway be
            treated as an array.*/
            var x2js = new X2JS({
                arrayAccessFormPaths: [
                    /.*\.Assoc$/
                ],
                escapeMode : true
            });
            var jsonObj = x2js.xml_str2json(myReader.result.replace(/\\n/g,"&#xa;")); // maybe this could be replaced by jquery code instead of this third party
            
            //$scope.concepts = []; // this is loaded implicitly in the function loadConceptsFromM1Tree
            $scope.categories.push(
                {
                    _name : "HBDM A and B"
                },
                {
                    _name : "Arrangement",
                    parentCategory: {
                        _identity: "HBDM A and B"
                    }
                },
                {
                    _name : "Business Direction Item",
                    parentCategory: {
                        _identity: "HBDM A and B"
                    }
                },
                {
                    _name : "Classification",
                    parentCategory: {
                        _identity: "HBDM A and B"
                    }
                },
                {
                    _name : "Condition",
                    parentCategory: {
                        _identity: "HBDM A and B"
                    }
                },
                {
                    _name : "Event",
                    parentCategory: {
                        _identity: "HBDM A and B"
                    }
                },
                {
                    _name : "Involved Party",
                    parentCategory: {
                        _identity: "HBDM A and B"
                    }
                },
                {
                    _name : "Location",
                    parentCategory: {
                        _identity: "HBDM A and B"
                    }
                },
                {
                    _name : "Product",
                    parentCategory: {
                        _identity: "HBDM A and B"
                    }
                },
                {
                    _name : "Resource Item",
                    parentCategory: {
                        _identity: "HBDM A and B"
                    }
                }
            );
            $scope.conceptTrees = $scope.conceptTrees.concat(loadConceptsFromM1Tree(jsonObj.HIERARCHY_DIAGRAM.Object,null));
        };

        // This function is called from m1WithRelationsXmlOnload when loading file from m1
        // It is also called from itself in a recursion
        var loadConceptsFromM1Tree = function (m1TreeRoot, parentReference) {
            // Basis of tree
            var conceptTree = {};
            conceptTree._rid = m1TreeRoot.InternalObjectId;
            /*conceptTree.id = conceptTree._rid;
            if (parentReference) {
                conceptTree.id += "-" + parentReference.id;
            }*/
            conceptTree.label = m1TreeRoot.ObjectName;
            // check if is already in the list of concepts
            var concept;
            var conceptAlreadyExisted = false;
            for (var i = 0; i < $scope.concepts.length; i++) {
                if ($scope.concepts[i]._rid == m1TreeRoot.InternalObjectId) {
                    concept = $scope.concepts[i];
                    conceptAlreadyExisted = true;
                    break;
                }
            }
            if (!conceptAlreadyExisted) {
                // Basis of list element
                concept = {};
                concept._rid = m1TreeRoot.InternalObjectId;
                concept._name = m1TreeRoot.ObjectName;
                //concept.customAttributes.customAttributeValue.attributeValue._value = m1TreeRoot.Unique_Id;
                var bid = m1TreeRoot.Unique_Id || '';
                concept.customAttributes = {
                    customAttributeValue: 
                        {
                            _customAttribute: "BID", 
                            attributeValue: { _value: bid }
                        } 
                };
                //concept.labels = {};
                //concept.labels.label = [];
                //concept.labels.label.push({});
                //concept.labels.label[0]._name = "HBDM"; //TODO: this is hard-coding and should be changed
                //concept.labels.label.push({});
                //concept.labels.label[1]._name = "concept";
                if (m1TreeRoot.ObjectTypeCode == 108) {
                    concept.labels = {};
                    concept.labels.label = [];
                    concept.labels.label.push({});
                    concept.labels.label[0]._name = "classifier";
                }
                //replacing line feeds in format \n from m1 to XML encoded
                //m1TreeRoot.Definition = m1TreeRoot.Definition.replace(/\\n/g,"&#xd;&#xa;");
                // dividing definition field into longDescription and shortDescription
                // use only shortDescription if it fits
                // if it doesnt fit:
                // 1. put the full description in longDescription
                // 2. take the first 251 characters of the full description, but remove the last word (to not cut in the middle of the word)
                //    ... and then add "..." in the end.
                if(m1TreeRoot.Definition.length > 255) {
                    concept._longDescription = m1TreeRoot.Definition;
                    var lastIndex = m1TreeRoot.Definition.substring(0,251).lastIndexOf(" ");
                    concept._shortDescription = m1TreeRoot.Definition.substring(0,lastIndex) + " ...";
                } else {
                    concept._shortDescription = m1TreeRoot.Definition;
                }
                // Assignment of Category
                // The m1 data doesnt support categories.
                // Therefore, the top two levels of concepts in the type hierarchy will be automatically assigned pre-configured categories.
                // The rest of the concepts will inherit the categories from the parent in the type hierarchy.
                concept.parentCategory = {};
                switch(concept._rid) {
                    case "4225":
                        concept.parentCategory._identity = "HBDM A and B";
                        break;
                    case "4186":
                        concept.parentCategory._identity = "HBDM A and B::Arrangement";
                        break;
                    case "4197":
                        concept.parentCategory._identity = "HBDM A and B::Business Direction Item";
                        break;
                    case "4207":
                        concept.parentCategory._identity = "HBDM A and B::Condition";                        
                        break;
                    case "4198":
                        concept.parentCategory._identity = "HBDM A and B::Classification";   
                        break;
                    case "4219":
                        concept.parentCategory._identity = "HBDM A and B::Event"; 
                        break;
                    case "4226":
                        concept.parentCategory._identity = "HBDM A and B::Involved Party"; 
                        break;
                    case "4227":
                        concept.parentCategory._identity = "HBDM A and B::Location"; 
                        break;
                    case "4228":
                        concept.parentCategory._identity = "HBDM A and B::Product";
                        break;
                    case "4229":
                        concept.parentCategory._identity = "HBDM A and B::Resource Item";
                        break;
                    default:
                        // the data is expected to have a parentReference in case it didnt match any of the names above
                        // otherwise there will be a null exception here...
                        concept.parentCategory = parentReference.parentCategory;
                }
            }
            if (parentReference != null) {
                // prepare for parent reference
                if(concept.isATypeOf == null) {
                    concept.isATypeOf = {};
                    concept.isATypeOf.termRef = [];
                }
                var termRef = {};
                termRef._rid = parentReference._rid;
                termRef._identity = parentReference._identity; //parentReference.parentName + "::" + parentReference.name;
                concept.isATypeOf.termRef.push(termRef);
            }
            // Add children to both the tree and list element           
            if (m1TreeRoot.Assoc != null && m1TreeRoot.Assoc.length > 0) {
                // prepare for child references
                concept.hasTypes = {};
                concept.hasTypes.termRef = [];
                conceptTree.children = [];
                for (var i = 0; i < m1TreeRoot.Assoc.length; i++) {
                    var referenceToThis = {};
                    //referenceToThis.id = m1TreeRoot.InternalObjectId;
                    //referenceToThis.id = conceptTree.id;
                    referenceToThis._rid = conceptTree._rid;
                    //referenceToThis.name = m1TreeRoot.ObjectName; // could be done nicer
                    referenceToThis.parentCategory = concept.parentCategory;
                    referenceToThis._identity = referenceToThis.parentCategory._identity + "::" + m1TreeRoot.ObjectName;
                    /*if (parentReference != null) {
                        //referenceToThis.parentName = parentReference.name;
                        referenceToThis._identity = parentReference._identity + "::" + m1TreeRoot.ObjectName;
                    } else {
                        referenceToThis._identity = referenceToThis.parentCategory._identity + "::" + m1TreeRoot.ObjectName;
                    }*/
                    conceptTree.children.push(loadConceptsFromM1Tree(m1TreeRoot.Assoc[i].Object, referenceToThis));
                    var termRef = {};
                    termRef._rid = m1TreeRoot.Assoc[i].Object.InternalObjectId;
                    //termRef._identity = m1TreeRoot.ObjectName + "::" + m1TreeRoot.Assoc[i].Object.ObjectName;
                    // check if child concept already exists - in that case the existing identity of that concept should be used.
                    var childConcept = getConceptByRID(termRef._rid);
                    if(childConcept != null)
                        termRef._identity = childConcept.parentCategory._identity + "::" + childConcept._name;
                    else
                        termRef._identity = referenceToThis.parentCategory._identity + "::" + m1TreeRoot.Assoc[i].Object.ObjectName;
                    concept.hasTypes.termRef.push(termRef);
                }
            }
            // add concept to list of concepts only if its not already there
            if (!conceptAlreadyExisted)
                $scope.concepts.push(concept);
            
            return conceptTree;
        };

        // This function is passed as a processorFunction(event) to the getOnLoadFunction
        // for loading from JSON format
        var glossaryToolJsonOnload = function (event) {
            var conceptSystem = JSON.parse(event.target.result);
            $scope.concepts = $scope.concepts.concat(conceptSystem.concepts);
            $scope.categories = $scope.categories.concat(conceptSystem.categories);
            if (conceptSystem.conceptTrees != null)
                $scope.conceptTrees = $scope.conceptTrees.concat(conceptSystem.conceptTrees);
        };

        // This function is passed as a processorFunction(event) to the getOnLoadFunction
        // for loading from IBM XML format
        // It transforms the list of concepts and categories as they are (just from XML to JSON)
        // It calls getConceptTree for the more complex logic to build the tree structure
        var ibmXmlOnload = function(event) {
            //console.log(myReader.result);
            /* x2js is the factory for transforming XML to JSON
            the arrayAccessFormPaths parameter is used to tell the parser which
            positions in the XML tree may repeated more than once.
            Then, even if they are only occurring once they will anyway be
            treated as an array.*/
            var x2js = new X2JS({
                arrayAccessFormPaths: [
                    "glossary.terms.term",
                    "glossary.categories.category",
                    "glossary.terms.term.labels.label",
                    "glossary.terms.term.isATypeOf.termRef",
                    "glossary.terms.term.hasTypes.termRef",
                    "glossary.terms.term.hasA.termRef",
                    "glossary.terms.term.isOf.termRef",
                    "glossary.terms.term.relatedTerms.termRef"
                ]
            });
            console.log("Data loading started.");
            var jsonObj = x2js.xml_str2json(myReader.result); // maybe this could be replaced by jquery code instead of this third party
            $scope.categories = $scope.categories.concat(jsonObj.glossary.categories.category);
            $scope.concepts = $scope.concepts.concat(jsonObj.glossary.terms.term);
            console.log("Data loaded as list.");
            // Remove all "_Assigned Assets for ..." of the Scopes category structure
            // It would have been better to include this in the xml2json transform, but this was easier.
            // I go from end to start in order to change the structure dynamically wo messing the index.
            for (var i = $scope.concepts.length - 1; i > -1; i--) {
                if ($scope.concepts[i].labels != null && $scope.concepts[i].labels.label[0]._name === "scope assigned assets") {
                    $scope.concepts.splice(i, 1);
                }
            }
            // free memory
            $scope.conceptSystem.file = null;
            // build tree for navigation
            var maxDepth = 10;
            for (var i = 0; i < $scope.concepts.length; i++) {
                if (!$scope.concepts[i].isOf && !$scope.concepts[i].isATypeOf) {
                    $scope.conceptTrees = $scope.conceptTrees.concat(getConceptTree($scope.concepts[i], maxDepth));
                }
            }
            console.log("Data transformed to tree.");
        };

        // This function takes a concept (from the list as imported from the IBM format) and assumes that this is a root.
        // It then loops through all concepts in the list $scope.concepts and adds the ones which are children to the root.
        // It doesnt add them directly but instead calls the getConceptTree for them so that their correspo
        // It is called by the function ibmXmlOnload and recursively by itself.
        // This implementation was chosen over another algorithm due to some inconsistencies in the data
        // The older, alternative (and maybe better) solution is to be found in an old version of the code.
        // Before this function was called getConceptTree2
        var getConceptTree = function (concept, maxDepth) {
            var conceptTree = {};
            conceptTree.label = concept._name;
            conceptTree._rid = concept._rid;
            /*conceptTree.id = conceptTree._rid;
            if(parentId)
                conceptTree.id += '-' + parentId;*/
            if (concept.parentCategory != null)
                conceptTree.parentCategoryRid = concept.parentCategory._rid;
            if (maxDepth < 2)
                return conceptTree;
            var childMaxDepth = maxDepth - 1;
            conceptTree.children = [];
            for (var i = 0; i < $scope.concepts.length; i++) {
                if ($scope.concepts[i].isATypeOf != null) {
                    for (var j = 0; j < $scope.concepts[i].isATypeOf.termRef.length; j++) {
                        if ($scope.concepts[i].isATypeOf.termRef[j]._rid === concept._rid) {
                            conceptTree.children.push(getConceptTree($scope.concepts[i], childMaxDepth));
                        }
                    }
                }
                if ($scope.concepts[i].isOf != null) {
                    for (var j = 0; j < $scope.concepts[i].isOf.termRef.length; j++) {
                        if ($scope.concepts[i].isOf.termRef[j]._rid === conceptTree.parentCategoryRid) {
                            conceptTree.children.push(getConceptTree($scope.concepts[i], childMaxDepth));
                        }
                    }
                }
            }
            return conceptTree;
        };

        var getCategoryTree = function (parentCategory) {
            var categoryTree = {};
            categoryTree.label = parentCategory._name;
            categoryTree.children = [];
            for (var i = 0; i < $scope.categories.length; i++) {
                if ($scope.categories[i].parentCategory != null &&
                    $scope.categories[i].parentCategory._rid === parentCategory._rid) {
                    categoryTree.children.push(getCategoryTree($scope.categories[i]));
                }
            }
            for (var i = 0; i < rootConcepts.length; i++) {
                if (rootConcepts[i].parentCategoryRid === parentCategory._rid) {
                    // splice removes the tree from the rootConcepts array and returns
                    // it so that can be pushed to the new array
                    categoryTree.children.push(rootConcepts[i]); //rootConcepts.splice[i]
                }
            }
            return categoryTree;
        };

        
        
         /*
         * 
         * ===================================== COMPARISON FUNCTIONALITY
         * 
         */
        
        
        // prepare comparison of concept sets
        $scope.comparison = {};
        $scope.comparison.leftCategory = "Business Terms";
        $scope.comparison.rightCategory = "HBDM";
        $scope.comparison.visualizeDifference = true;
        $scope.compareConceptSets = function () {
            var differenceReport = getDiffReportForSets(
                                        getConceptsByCategory($scope.comparison.leftCategory),
                                        getConceptsByCategory($scope.comparison.rightCategory));
            //var symmetricDifferenceJson = [];
            var symmetricDifferenceJson = {
                'terms' : {
                    'term' : []   
                }
            };
            symmetricDifferenceJson.terms.term = addConceptsFromSubsetRidArrayToSymmetricDiffJson(
                                                differenceReport.onlyInLeft, 
                                                $scope.comparison.leftCategory,
                                                symmetricDifferenceJson.terms.term);
            symmetricDifferenceJson.terms.term = addConceptsFromSubsetRidArrayToSymmetricDiffJson(
                                                differenceReport.onlyInRight, 
                                                $scope.comparison.rightCategory,
                                                symmetricDifferenceJson.terms.term);                                                
            // $scope.comparison.symmetricDifferenceXmlUrl = getXmlUrl(symmetricDifferenceJson);
            var symmetricDifferenceXmlUrl = getXmlUrl(symmetricDifferenceJson);
            triggerDownload(symmetricDifferenceXmlUrl, "symmetric-differences.xml");
            var intersectionJson = {
                'similarities' : {
                    'similarity' : []   
                }
            };
            //var intersectionJson = {};
            //intersectionJson.similarities = [];
            var tmpConceptLeft,tmpConceptRight,outputRecord;
            for(var i = 0; i < differenceReport.similarities.length; i++) {
                tmpConceptLeft = getConceptByRID(differenceReport.similarities[i].leftConceptSystemId);
                tmpConceptRight = getConceptByRID(differenceReport.similarities[i].rightConceptSystemId);
                outputRecord = {};
                outputRecord.BID = tmpConceptLeft.customAttributes.customAttributeValue.attributeValue._value;
                outputRecord.parentCategory_A = tmpConceptLeft.parentCategory._identity;
                outputRecord.name_A = getEnglishPreferredTerm(tmpConceptLeft);
                outputRecord.description_A = $scope.getLongestDescription(tmpConceptLeft);
                outputRecord.parentCategory_B = tmpConceptRight.parentCategory._identity;
                outputRecord.name_B = getEnglishPreferredTerm(tmpConceptRight);
                outputRecord.description_B = $scope.getLongestDescription(tmpConceptRight);
                outputRecord.haveDiffName = differenceReport.similarities[i].haveDiffName;
                outputRecord.haveDiffDesc = differenceReport.similarities[i].haveDiffDesc;
                outputRecord.haveDiffPlaces = differenceReport.similarities[i].haveDiffPlaces;
                intersectionJson.similarities.similarity.push(outputRecord);
            }
            //$scope.comparison.intersectionXmlUrl = getXmlUrl(intersectionJson);
            var intersectionXmlUrl = getXmlUrl(intersectionJson);
            triggerDownload(intersectionXmlUrl, "intersection.xml");
            if($scope.comparison.visualizeDifference)
                visualizeComparisonInTree(differenceReport);
        };
        
        var addConceptsFromSubsetRidArrayToSymmetricDiffJson = function(subsetRidArray,subsetName,symmetricDifferenceJson) {
            var tmpConcept,outputConcept;
            for(var i = 0; i < subsetRidArray.length ; i++) {
                tmpConcept = getConceptByRID(subsetRidArray[i]);
                outputConcept = {};
                outputConcept.subsetName = subsetName;
                outputConcept.parentCategory = tmpConcept.parentCategory._identity;
                outputConcept.BID = tmpConcept.customAttributes.customAttributeValue.attributeValue._value;
                outputConcept.name = getEnglishPreferredTerm(tmpConcept);
                outputConcept.description = $scope.getLongestDescription(tmpConcept);
                symmetricDifferenceJson.push(outputConcept);
            }
            return symmetricDifferenceJson;
        };
        
        

        
        var shareAtLeastOneParent = function(leftConcept,rightConcept) {
            if(
                leftConcept.isATypeOf != null && leftConcept.isATypeOf.termRef != null &&
                rightConcept.isATypeOf != null && rightConcept.isATypeOf.termRef != null
            ) {
                var tmpLeftConceptParent,tmpRightConceptParent;
                for(var i = 0; i < leftConcept.isATypeOf.termRef.length; i++) {
                    tmpLeftConceptParent = getConceptByRID(leftConcept.isATypeOf.termRef[i]._rid);
                    for(var j = 0; j < rightConcept.isATypeOf.termRef.length; j++) {
                        tmpRightConceptParent = getConceptByRID(rightConcept.isATypeOf.termRef[j]._rid);
                        if(getConceptBID(tmpLeftConceptParent) == getConceptBID(tmpRightConceptParent)) {
                            return true;
                        }
                    }
                }
            }
            return false;
        };
        
        // common function to prepare both name and description before equality comparison
        // replaces abbrevation with full denomination
        // trims leading or trailing spaces
        // removes spaces around forward slash character
        // replace 'non-xxx' with 'non xxx' (in a future transformation the other way would probably be used)
        var getStringForCompare = function(str) {
           var abbreviations = {
              'AR': 'Arrangement',
              'BD': 'Business Direction Item',
              'CD': 'Condition',
              'CL': 'Classification',
              'EV': 'Event',
              'IP': 'Involved Party',
              'LO': 'Location',
              'PD': 'Product',
              'RI': 'Resource Item',
              'LC': 'Life Cycle',
              'Acctg': 'Accounting',
              'EMPLMT': 'Employment',
              'Org': 'Organization',
              'POSTN': 'Position',
              'RLTNP': 'Relationship',
              'Chng': 'Change',
              'Reasn': 'Reason',
              'Ctgry': 'Category',
              'Dvlpmnt': 'Development',
              'Info': 'Information',
              'Mktg': 'Marketing',
              'Prspctv': 'Prospective',
              'CPCTY': 'Capacity',
              'LGL': 'Legal',
              'Admtv': 'Administrative',
              'SGNTR': 'Signature',
              'CRTFCTN': 'Certification',
              'Exchng': 'Exchange',
              'Off-Bal': 'Off Balance',
              'FNCL': 'Financial',
              'MKT': 'Market',
              'OFFRG': 'Offering',
              'DCLRTN': 'Declaration',
              'DOCMTN': 'Documentation',
              'GOVT': 'Government',
              'EBO': 'Elementary Business Operation'
            };
            for (var key in abbreviations) {
              var re = new RegExp('\\b' + key + '\\b', 'gi');
              str = str.replace(re, abbreviations[key]);
            }
            return str.trim().replace(" \/ ","\/").replace(/-/g, ' ');
        };
        
        var getConceptDescForCompare = function(concept) {
            var desc;
            if(concept._longDescription !=null && concept._longDescription.length > 0)
                desc = concept._longDescription;
            else
                desc = concept._shortDescription;
            if(conceptHasLabel(concept,"classifier")) {
                // if the term is "classifier" remove first three words before comparing
                // "A Classification Scheme that distinguishes" and "A term that distinguishes ..."
                desc = desc.split('distinguishes').pop();
            } else {
                // not classifier so then we can remove the first sentence of the description,
                // like "A Value of ARRANGEMENT CREDIT LOCATION." for concepts coming from an early edition of the banking model
                // (i.e. HBDM)
                var redundantStart = "A Value of";
                if(desc.substring(0,redundantStart.length) == redundantStart)
                    desc = desc.replace(/^([^\.]*)\.*/, '').trim();   
            }
            return getStringForCompare(desc);
        };
        
        var getConceptNameForCompare = function(concept) {
            var name = getEnglishPreferredTerm(concept);
            name = getStringForCompare(name);
            // removes additional whitespaces, removes any text in parantheses, to lower case
            return name.toLowerCase().replace(/ *\([^)]*\) */g, "");
        };
        
        var getDiffReportForSets = function(leftSet,rightSet) {
            var differenceReport = {};
            differenceReport.leftSize = leftSet.length;
            differenceReport.rightSize = rightSet.length;
            differenceReport.onlyInLeft = [];
            differenceReport.onlyInRight = [];
            differenceReport.similarities = [];
            differenceReport.getOnlyInLeftCount = function() {
                 return differenceReport.onlyInLeft.length;
            };
            differenceReport.getOnlyInRightCount = function() {
                 return differenceReport.onlyInRight.length;
            };
            differenceReport.getInBothCount = function() {
                return differenceReport.similarities.length;
            };
            differenceReport.getInBothWithDiffNameCount = function() {
                var inBothWithDiffNameCount = 0;
                if(differenceReport.similaries != null) {
                    for(var i = 0; i < differenceReport.similaries.length; i++) {
                        if(differenceReport.similaries[i].haveDiffName) {
                            inBothWithDiffNameCount++;
                        }                    
                    }
                }
                return inBothWithDiffNameCount;
            };
            differenceReport.getInBothWithDiffDescCount = function() {
                var inBothWithDiffDescCount = 0;
                if(differenceReport.similaries != null) {
                    for(var i = 0; i < differenceReport.similaries.length; i++) {
                        if(differenceReport.similaries[i].haveDiffDesc) {
                            inBothWithDiffDescCount++;
                        }                    
                    }
                }
                return inBothWithDiffDescCount;
            };
            differenceReport.getInBothWithDiffPlacesCount = function() {
                var inBothWithDiffPlacesCount = 0;
                if(differenceReport.similaries != null) {
                    for(var i = 0; i < differenceReport.similaries.length; i++) {
                        if(differenceReport.similaries[i].haveDiffPlaces) {
                            inBothWithDiffPlacesCount++;
                        }                    
                    }
                }
                return inBothWithDiffPlacesCount;
            };
            for(var i = 0; i < leftSet.length; i++) {
                var isInBoth = false;
                for(var j = 0; j < rightSet.length; j++) {
                    if( getConceptBID(leftSet[i]) == getConceptBID(rightSet[j]) ) {
                        isInBoth = true;
                        var similarity = {};
                        similarity.leftConceptSystemId = leftSet[i]._rid;
                        similarity.rightConceptSystemId = rightSet[j]._rid;
                        similarity.haveDiffName = ( 
                                                    getConceptNameForCompare(leftSet[i]) != 
                                                    getConceptNameForCompare(rightSet[j]) );                    

                        similarity.haveDiffDesc = ( getConceptDescForCompare(leftSet[i]) != getConceptDescForCompare(rightSet[j]) );
                        similarity.haveDiffPlaces = !( shareAtLeastOneParent(leftSet[i],rightSet[j]) );
                        differenceReport.similarities.push(similarity);
                        break; //there may only be one of each id in each set
                    }
                }
                if(!isInBoth) {
                    differenceReport.onlyInLeft.push(leftSet[i]._rid);
                }
            }
            for(var i = 0; i < rightSet.length; i++) {
                var rightIsAlsoInLeft = false;
                for(var j = 0; j < differenceReport.similarities.length; j++) {
                    if(rightSet[i]._rid == differenceReport.similarities[j].rightConceptSystemId) {
                        rightIsAlsoInLeft = true;
                        break;
                    }
                }
                if(!rightIsAlsoInLeft) {
                    differenceReport.onlyInRight.push(rightSet[i]._rid);
                }
            }
            return differenceReport;
		};
        
        var visualizeComparisonInTree = function(differenceReport) {
            var allNewConcepts = differenceReport.onlyInLeft.concat(differenceReport.onlyInRight);
            for(var i = 0;allNewConcepts.length>i;i++) {
                var allNewTreeNodes = 
                    findAllConceptTreeNodesByRid($scope.conceptTrees, allNewConcepts[i]);
                for(var j = 0;j<allNewTreeNodes.length;j++) {
                    allNewTreeNodes[j].comparison = {};
                    allNewTreeNodes[j].comparison.isNew = true;
                }
                //console.log("Visualizing new concept " + i + " of " + allNewConcepts.length);
            }
            for(var k = 0;differenceReport.similarities.length>k;k++) {
                var similarTreeNodes = 
                    findAllConceptTreeNodesByRid($scope.conceptTrees, differenceReport.similarities[k].leftConceptSystemId);
                similarTreeNodes = similarTreeNodes.concat(findAllConceptTreeNodesByRid($scope.conceptTrees, differenceReport.similarities[k].rightConceptSystemId));
                for(var l = 0;l<similarTreeNodes.length;l++) {
                    similarTreeNodes[l].comparison = {};
                    similarTreeNodes[l].comparison.haveDiffName = differenceReport.similarities[k].haveDiffName;
                    similarTreeNodes[l].comparison.haveDiffDesc = differenceReport.similarities[k].haveDiffDesc;
                    similarTreeNodes[l].comparison.haveDiffPlaces = differenceReport.similarities[k].haveDiffPlaces;
                }
                //console.log("Visualizing similarity " + k + " of " + differenceReport.similarities.length);
            }            
 
        };
        
        $scope.comparison.alreadyHandledBidsCsv = '';
        $scope.updateHandledBidsInTree = function() {
        	var alreadyHandledBids = $scope.comparison.alreadyHandledBidsCsv.split(",");
        	// remove any duplicates and remove duplicate values
        	alreadyHandledBids = alreadyHandledBids.filter(
        	   function(value, index) {
        	       return alreadyHandledBids.indexOf(value) == index && value.length>0;
        	   }
        	);
        	// transform bids array to rids array since the tree structure only uses rids (for now!)
        	var alreadyHandledRids = [];
        	for(var i=0;i<alreadyHandledBids.length;i++) {
        		alreadyHandledRids = alreadyHandledRids.concat(getRidsFromBid($scope.concepts,alreadyHandledBids[i]));
        	}
        	// update tree structure with the new data TODO: rewrite using map/reduce/filter/foreach
        	var allNewTreeNodes = [];
        	for(var j=0;j<alreadyHandledRids.length;j++) {
        		allNewTreeNodes = allNewTreeNodes.concat(findAllConceptTreeNodesByRid($scope.conceptTrees,alreadyHandledRids[j]));
        	}
        	// reset all tree nodes
        	// using a function in ivhTreeviewMgr TODO: this function could be used in more places traversing the tree
        	/*ivhTreeviewMgr.ivhTreeviewBfs($scope.conceptTrees, undefined, function(node) {
                if( node.comparison && 
                    node.comparison.haveDiffName === false && 
                    node.comparison.haveDiffDesc === false &&
                    node.comparison.haveDiffPlaces === false
                )
                    node.alreadyHandled = true;
                else
                    node.alreadyHandled = false;
                return true; // to make the traversal continue
            });*/
        	
        	// update tree structure with the new data
        	allNewTreeNodes.forEach(function(node) {
                node.alreadyHandled = true;
            });
        };
        
         /*
         * 
         * ===================================== EXPORTING DATA
         * 
         */
        
        // the statement JSON.parse(angular.toJson(jsonObject)) comes from
        // https://stackoverflow.com/questions/32344495/remove-hashkey-from-array
        // and is there to strip the json object from angular objects $$hashKey
        var getXmlUrl = function(jsonObject) {
            var x2js = new X2JS({ escapeMode: true });
            var json2 = angular.toJson(jsonObject);
            var json2parsed = JSON.parse(json2);
            var xmlObject = x2js.json2xml_str( json2parsed );
            var blob = new Blob([xmlObject.replace(/\n/g,"&#xa;")], { type: 'text/xml' });
            return (window.URL || window.webkitURL).createObjectURL(blob); 
        };

        $scope.exportConceptSystem = function() {
            // clean all search phrase match score tags
              deleteAllAttributesInObject($scope.concepts,'searchPhraseMatchScore');
              switch ($scope.conceptSystem.format) {
                case "1":
                    triggerDownload(getConceptSystemIGCXMLUrl(),"concept-system-igc.xml");
                    break;
                case "4":
                    triggerDownload(getConceptSystemJSONUrl(),"concept-system.json");
                    break;
                default:
                    triggerDownload(getConceptSystemJSONUrl(),"concept-system.json");
            }            
        };
        
        // pattern from http://blog.neilni.com/2016/04/23/download-file-in-angular-js/
        // NOTE: document is the html DOM document
        var triggerDownload = function(url,filename) {
            var a = document.createElement('a');
            a.href = url;
            a.download = filename;
            a.target = '_blank';
            a.click();
        };
        

        var getConceptSystemJSONUrl = function () {
            var conceptSystem = {};
            conceptSystem.concepts = copy($scope.concepts);
            conceptSystem.categories = copy($scope.categories);
            conceptSystem.conceptTrees = copy($scope.conceptTrees);
            // check if user decided to strip the RIDs
            if(!$scope.conceptSystem.includeRid) {
                // the file will not work to import again
                // for enable such import of files without RID
                // a RID automatic assignment functionality needs to be implemented
                deleteAllAttributesInObject(conceptSystem,'_rid');
                //conceptSystem.concepts.forEach(deleteRid);
                //conceptSystem.categories.forEach(deleteRid);                
            }
            var theJSON = angular.toJson(conceptSystem);
            var blob = new Blob([theJSON], { type: 'text/json' });
            return (window.URL || window.webkitURL).createObjectURL(blob);
        };
        
        var getConceptSystemIGCXMLUrl = function () {
            var jsonObj = {};
            /*jsonObj = {
                'm:glossary' : {
                    '_tns:m' : 'http://www.ibm.com/is/bg/importexport'   
                }
            };*/
            jsonObj.glossary = {};
            jsonObj.glossary._xmlns = "http://www.ibm.com/is/bg/importexport";
            jsonObj.glossary._version = "1.6";
            jsonObj.glossary.terms = {};
            jsonObj.glossary.terms.term = copy($scope.concepts);
            jsonObj.glossary.categories = {};
            jsonObj.glossary.categories.category = copy($scope.categories);
            if(!$scope.conceptSystem.includeRid) {
                // the file will not work to import again
                // for enable such import of files without RID
                // a RID automatic assignment functionality needs to be implemented
                deleteAllAttributesInObject(jsonObj.glossary,'_rid');
                //jsonObj.glossary.terms.term.forEach(deleteRid);
                //jsonObj.glossary.categories.category.forEach(deleteRid);                
            }  
            return getXmlUrl(jsonObj);
        };
        
                // Functions supporting the CSV export feature
        //$scope.getCsvFilename = "concepts.csv";
        $scope.getCsvHeader = ["Type",
            "Name",
            "Notes",
            "CSV_KEY",
            "CSV_PARENT_KEY"
        ];
        $scope.getCsvData = function () {
            //filteredConcepts = $filter('conceptFilterAndOrder')($scope.concepts, $scope.filter);
            var csv = [];
            for (var i = 0; i < $scope.categories.length; i++) { //[{a:1,b:2},{a:3,b:4}]
                var category = $scope.categories[i];
                var row = [];
                row.push("Package");
                row.push(category._name);
                row.push(category._shortDescription);
                row.push(category._rid);
                if (category.parentCategory) {
                    row.push(category.parentCategory._rid);
                } else {
                    row.push(null);
                }
                csv.push(row);
            }
            for (var i = 0; i < $scope.concepts.length; i++) { //[{a:1,b:2},{a:3,b:4}]
                var concept = $scope.concepts[i];
                row = [];
                row.push("Class");
                row.push(concept._name);
                row.push(concept._shortDescription);
                row.push(concept._rid);
                if (concept.parentCategory) {
                    row.push(concept.parentCategory._rid);
                } else {
                    row.push(null);
                }
                csv.push(row);
            }
            return csv;
        };

         /*
         * 
         * ===================================== COMMON FUNCTIONALITY
         * 
         */

        var getConceptByRID = function (rid) {
            for (var i = 0; i < $scope.concepts.length; i++) {
                if ($scope.concepts[i]._rid === rid ) {
                    return $scope.concepts[i];
                }
            }
        };
        
        $scope.getEnglishPreferredTerm = function(concept) {
            return concept._name;
            //return concept.name.__text;
        };

        $scope.getTwoMostInnerLevels = function(fullyQualifiedEntity) {
            var allLevels = fullyQualifiedEntity.split("::");
            var depth = allLevels.length;
            var twoMostInnerLevels = {};
            twoMostInnerLevels.inner = allLevels[depth - 1];
            twoMostInnerLevels.outer = allLevels[depth - 2];
            return twoMostInnerLevels;
            //return allLevels[depth - 2] + "::" + allLevels[depth - 1];
        };

        $scope.getTwoMostInnerLevelsAsString = function (fullyQualifiedEntity) {
            var allLevels = fullyQualifiedEntity.split("::");
            var depth = allLevels.length;
            return allLevels[depth - 2] + "::" + allLevels[depth - 1];
        };


 
         var conceptHasLabel = function(concept,labelName) {
            for (var j = 0; j < concept.labels.label.length; j++) {
                if(concept.labels.label[j]._name == labelName) {
                    return true;
                }
            }
            return false;
        };
        
        var getConceptsByLabel = function(labelName) {
            var concepts = [];
            for (var i = 0; i < $scope.concepts.length; i++) {
                if(conceptHasLabel($scope.concepts[i])) {
                    concepts.push($scope.concepts[i]);
                }
            }
            return concepts;
        };
         
        var getConceptsByCategory = function(category) {
            var concepts = [];
            for (var i = 0; i < $scope.concepts.length; i++) {
                if( $scope.concepts[i].parentCategory._identity.indexOf(category) !== -1 ) {
                    concepts.push($scope.concepts[i]);
                }
            }
            return concepts;
        };
        
        // check if null
        // pattern from https://stackoverflow.com/questions/2631001/javascript-test-for-existence-of-nested-object-key
        var getConceptBID = function(concept) {
            return ((((concept || {}).customAttributes || {}).customAttributeValue || {}).attributeValue || {})._value;
        };
        
        var findAllConceptTreeNodesByRid = function(conceptTrees,rid) {
            var results = [];
            for(var j=0;j<conceptTrees.length;j++) {
                if(conceptTrees[j]._rid == rid) {
                   results.push(conceptTrees[j]);
                }
                if(conceptTrees[j].children) {
                    var childMatches = findAllConceptTreeNodesByRid(conceptTrees[j].children,rid);
                    if(childMatches)
                        results = results.concat(childMatches);
                }
            }
            return results;
        };
        
        var getRidsFromBid = function(concepts,bid) {
        	var rids = concepts.reduce(function(filtered, concept) {
        	  if (getConceptBID(concept) === bid) {
        		 filtered.push(concept._rid);
        	  }
        	  return filtered;
        	}, []);
        	return rids;
        };
 
        $scope.getLongestDescription = function(concept) {
            if(concept._longDescription)
                return concept._longDescription;
            if(concept._shortDescription)
                return concept._shortDescription;
        };
        
        // used when exporting without Rid
        var deleteRid = function(glossaryEntity) {
            delete glossaryEntity._rid;
        };
        
        // deletes all nested properties of name attributeName in the object theObject
        // example: deleteAllAttributesInObject($scope.conceptSystem,'_rid')
        // used by the export feature
        // pattern from https://stackoverflow.com/questions/15523514/find-by-key-deep-in-a-nested-object
        var deleteAllAttributesInObject = function(theObject,attributeName) {
            if(theObject instanceof Array) {
                for(var i = 0; i < theObject.length; i++) {
                    deleteAllAttributesInObject(theObject[i],attributeName);
                }
            } else {
                for(var prop in theObject) {
                    if(prop == attributeName) {
                        delete theObject[prop];
                    }
                    if(theObject[prop] instanceof Object || theObject[prop] instanceof Array) {
                        deleteAllAttributesInObject(theObject[prop],attributeName);
                    }
                }
            }
        };
        
        // deep cloning of javascript array or object
        // used when exporting data and needing to manipulate the data before exporting
        // pattern from https://stackoverflow.com/questions/7486085/copying-array-by-value-in-javascript
        var copy = function(o) {
          var output, v, key;
          output = Array.isArray(o) ? [] : {};
          for (key in o) {
            v = o[key];
            output[key] = (typeof v === "object" && v !== null) ? copy(v) : v;
          }
          return output;
        };

        /*
         * 
         * ==================================== PAGING OF RESULTS AND FILTERING
         * 
         */

        var initialResultCountLimit = 25;
        $scope.resultCountLimit = initialResultCountLimit;
        $scope.resultCountIncrement = 25;
        $scope.increaseResultCountLimit = function () {
            $scope.resultCountLimit += $scope.resultCountIncrement;
            console.log("resultCountLimit " + $scope.resultCountLimit);
        };

        $scope.$watch('filter', function () {
            $scope.resultCountLimit = initialResultCountLimit;
        }, true); // true means check deep not only the reference of the object

        // split(",") is the inverse of join() - splitting string of comma-separated words into an array
        // NOTE: using this method it is not allowed to use commas in any names of domains, branches etc.!!!
        // a validation program should be run with some period checking the data quality of the repository
        $scope.filter = {};
        //$scope.filter.granularities = new Object();
        //$scope.filter.granularities.highlevel = false;
        //$scope.filter.granularities.detailed = true;
        if ($routeParams.q) {
            $scope.filter.searchPhrase = decodeURIComponent($routeParams.q);
        }
		/*if($routeParams.hl == "true") {
			$scope.filter.granularities = decodeURIComponent($routeParams.g).split(",");
		} else {
			$scope.filter.granularities = ["Detailed"];
		} */
        $scope.filter.granularities = {};
        $scope.filter.granularities.highLevel = false;
        $scope.filter.granularities.detailed = true;
        if ($routeParams.d) {
            $scope.filter.domains = decodeURIComponent($routeParams.d).split(",");
        } else {
            $scope.filter.domains = ["All"];
        }
		/*if($routeParams.t) {
			$scope.filter.temporalities = decodeURIComponent($routeParams.t).split(",");
		} else {
			$scope.filter.temporalities = ["Current"];
		}*/
        $scope.filter.temporalities = {};
        $scope.filter.temporalities.inUse = true;
        $scope.filter.temporalities.planned = false;
        if ($routeParams.b) {
            $scope.filter.branches = decodeURIComponent($routeParams.b).split(",");
        } else {
            $scope.filter.branches = ["All"];
        }

          /*
         * 
         * ===================================== PRESENTATION FUNCTIONALITY
         * 
         */

		// code copy
		// the replace operation is there to convert \n etc from the source
		// to '<br>' (maybe this should be done in the backend actually)
		// http://stackoverflow.com/questions/14948223/how-to-convert-n-to-html-line-break
		// added \\n myself to support explicit encoding using the characters \n to represent line break
	    $scope.to_trusted = function(html_code) {
    		//return $sce.trustAsHtml(html_code.replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n|\\n)/g, '$1' + '<br>' + '$2'));
    		if(html_code) {
    		  return $sce.trustAsHtml(html_code.replace(/(\r\n|\n\r|\r|\n)/g, '<br>'));
    		  //return $sce.trustAsHtml(html_code);
    		}
    		else {
    		  return '';
    		}
		};

		$scope.removeHtmlTags = function(html_code) {
			// Strip the HTML from the result http://stackoverflow.com/questions/822452/strip-html-from-text-javascript
			var tmp = document.createElement("DIV");
			tmp.innerHTML = html_code;
			return tmp.textContent || tmp.innerText || "";
		};

		// This code could may be used also in the html views (at the moment there is some duplication of logic)
		// Moreover, it could probably be done in some more robust way.
		// Now, still, it is robust against changes in protocol and hostname.
		$scope.getConceptAbsoluteUrl = function(conceptId) {
			return $location.protocol() + ":\/\/" + $location.host() + "\/derive-glossary-frontend\/index.html#concepts\/" + conceptId;
        };

          /*
         * 
         * ===================================== EDITING FUNCTIONALITY
         * 
         */

		// the concept name is stored in several places
		// both in hierarchy and in the list
		// in the hierarchy it may appear multiple times
		// in the list it may appear also as linked from other concepts
		// TODO: this method is crazy since the data is not normalized
		$scope.updateConceptName = function(concept, newName) {
		    concept._name = newName; // is already done as per the current design... TODO: improve
		    // in the list it may appear also as linked from other concepts
		    var firstHandConceptReferences = getFirstHandConceptReferences(concept);
		    for(var i=0;i<firstHandConceptReferences.length;i++) {
                // for each related concept we now need to go through the related concepts of THAT concept
                // so that we can change the reference to ourselves (!!)
                var firstHandConcept = getConceptByRID(firstHandConceptReferences[i]._rid);
                var secondHandConceptReferences = getFirstHandConceptReferences(firstHandConcept);
                for(var q=0;q<secondHandConceptReferences.length;q++) {
                    // check if one of the second references are ourselves...
                    if(secondHandConceptReferences[q]._rid == concept._rid) {
                        updateMostInnerLevelOfIdentity(secondHandConceptReferences[q],newName);
                    }
                }
		    }
		    // in the hierarchy it may appear multiple times
		    var matchingTreeConcepts = findAllConceptTreeNodesByRid($scope.conceptTrees,concept._rid);
            for(var j=0; j<matchingTreeConcepts.length;j++) {
                matchingTreeConcepts[j].label = newName;
            }
		};
		
		// TODO: this implementation is risky since it assumes that there may be only
		// a specific set of relationship types
		var getFirstHandConceptReferences = function(concept) {
		    var conceptReferences = [];
		    // it is the references which are pushed into the array
		    // https://stackoverflow.com/questions/8660901/do-objects-pushed-into-an-array-in-javascript-deep-or-shallow-copy
		    if(concept.isATypeOf) {
		      conceptReferences = conceptReferences.concat(concept.isATypeOf.termRef);
		    }
		    if(concept.hasTypes) {
		      conceptReferences = conceptReferences.concat(concept.hasTypes.termRef);
		    }
		    if(concept.hasA) {
		      conceptReferences = conceptReferences.concat(concept.hasA.termRef);
		    }
		    if(concept.isOf) {
		        conceptReferences = conceptReferences.concat(concept.isOf.termRef);
		    }
		    if(concept.relatedTerms) {
		      conceptReferences = conceptReferences.concat(concept.relatedTerms.termRef);
		    }
		    return conceptReferences;
		};
		
		// the concept which is passed is updated
		var updateMostInnerLevelOfIdentity = function (concept,newMostInnerLevelOfIdentity) {
            var allLevels = concept._identity.split("::");
            var depth = allLevels.length;
            allLevels[depth - 1] = newMostInnerLevelOfIdentity;
            concept._identity = allLevels.join("::");
        }; 
 
         /*
         * 
         * ===================================== TRANSFORMATION FUNCTIONALITY
         * 
         */
        
        //transformation functionality
        $scope.transformation = {};
        $scope.transformation.type = "1";
        $scope.transformation.category = 'HBDM';
        $scope.transformConceptSet = function() {
            var conceptsToTransform = getConceptsByCategory($scope.transformation.category);
            switch ($scope.transformation.type) {
                case "1":
                    transformTermsToLowerCase(conceptsToTransform);
                    break;
            }
        };
        
        // all non-appellations are transformed to lower case
        var transformTermsToLowerCase = function(conceptsToTransform) {
            console.log("Running transformation...");
            for (var i = 0 ;i < conceptsToTransform.length; i++) {
                //TODO: add appellation check!!
                $scope.updateConceptName(conceptsToTransform[i], conceptsToTransform[i]._name.toLowerCase());
            }
        };
        
        //var getFirstMatchingConceptTreeNodeId = function(concept) {
          //  return 
        //};

}]);

glossaryControllers.controller('ConceptDetailCtrl', ['$scope', '$sce', '$routeParams','$http', '$q','$location', '$anchorScroll', '$window','ivhTreeviewMgr',
	function($scope,$sce,$routeParams,$http,$q,$location, $anchorScroll,$window,ivhTreeviewMgr) {

        var findFirstTreeNodeByRid = function(conceptTrees,rid) {
            for(var j=0;j<conceptTrees.length;j++) {
                if(conceptTrees[j]._rid == rid) {
                   return conceptTrees[j];
                }
                if(conceptTrees[j].children) {
                    var firstTreeNode = findFirstTreeNodeByRid(conceptTrees[j].children,rid);
                    if(firstTreeNode)
                        return firstTreeNode;
                }
            }
            return null;
        };


		$scope.$watch('dataLoaded', function(dataLoaded) {
			if (dataLoaded) {
				// now we have retrieved all concepts, so lets pick then one specified in the route
				var concepts = $.grep($scope.$parent.concepts, function(e){ return e._rid === $routeParams.conceptId; });
				
				// ensure we got a hit
				if (concepts.length === 0) {
				  // not found - should not happen in good case (will add handling later)
				} else if (concepts.length === 1) {
					// access the concept match property
					$scope.concept = concepts[0];
					// open tree at location
                    ivhTreeviewMgr.deselectAll($scope.conceptTrees);
                    //ivhTreeviewMgr.collapseRecursive($scope.conceptTrees);
                    var treeNode = findFirstTreeNodeByRid($scope.conceptTrees, $scope.concept._rid);
                    ivhTreeviewMgr.expandTo($scope.conceptTrees, treeNode);
                    ivhTreeviewMgr.select($scope.conceptTrees, treeNode);
				} else {
				  // multiple items found - should not happen in case data isn't corrupted (will add handling later)
				}
			}
		});
		
		// ugly solution
		$scope.searchPhraseTouched = 0;
		//$scope.filterSomewhatInitialized = false;
		$scope.$watch('filter.searchPhrase', function() {
			$scope.searchPhraseTouched ++;
			// the search phrase is touched when the site is initialized, so therefore I had to set a threshold
			if($scope.searchPhraseTouched > 1) {
				$window.location.href = "#concepts";
				//$scope.hej++;
			}
		}, true); // true means check deep not only the reference of the object

}]);