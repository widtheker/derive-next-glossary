var glossaryApp = angular.module('glossaryApp', [
	'glossaryControllers',
	'ngRoute',
	'isteven-multi-select', // ALT 1
	//"angular-multi-select", // ALT 2
    'file-model', // for file import
	'ngSanitize',
    'ngCsv',
    'ngResource',
    'ivh.treeview',
    'angular-button-spinner',
    'xeditable'
]);

glossaryApp.config(['$compileProvider',
    function ($compileProvider) {
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|file|blob):/);
    }]);
    
// used for third-party library xeditable
glossaryApp.run(['editableOptions','editableThemes', function(editableOptions,editableThemes) {
  editableThemes.bs3.inputClass = 'input-sm';
  editableThemes.bs3.buttonsClass = 'btn-sm';
  editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
}]);

glossaryApp.config(['$routeProvider',//'$locationProvider',
	function($routeProvider/*,$locationProvider*/) {
		$routeProvider.
			when('/concepts', {
				templateUrl: 'partials/concepts.html',
				//controller: 'ConceptsCtrl'
			}).
			when('/concepts/:conceptId', {
				templateUrl: 'partials/concept-detail.html',
				controller: 'ConceptDetailCtrl'
            }).
            when('/systems/import', {
                templateUrl: 'partials/system-import.html',
                //controller: 'ConceptSystemCtrl'
            }).
            when('/systems/export', {
                templateUrl: 'partials/system-export.html',
                //controller: 'ConceptSystemCtrl'
            }).
            when('/compare', {
                templateUrl: 'partials/compare.html',
                //controller: 'ConceptSystemCtrl'
            }).
            when('/transform', {
                templateUrl: 'partials/transform.html',
                //controller: 'ConceptSystemCtrl'
            }).
			when('/about', {
				templateUrl: 'partials/about.html',
				//controller: 'ConceptsCtrl'
			}).
			otherwise({
				redirectTo: '/concepts'
			});
			//$locationProvider.html5Mode(true);
	}
]);

glossaryApp.config(function (ivhTreeviewOptionsProvider) {
    ivhTreeviewOptionsProvider.set({
        //twistieCollapsedTpl: '<span class="glyphicon glyphicon-chevron-right"></span>',
        //twistieCollapsedTpl: '<span class="glyphicon glyphicon-folder-close"></span>',
        twistieCollapsedTpl: '<span class="glyphicon glyphicon-triangle-right"></span>',
        //twistieExpandedTpl: '<span class="glyphicon glyphicon-chevron-down"></span>',
        //twistieExpandedTpl: '<span class="glyphicon glyphicon-folder-close"></span>',
        twistieExpandedTpl: '<span class="glyphicon glyphicon-triangle-bottom"></span>',
        //twistieLeafTpl: '&#9679;',
        twistieLeafTpl: '<span class="glyphicon glyphicon-minus"></span>',
        defaultSelectedState: false,
        disableCheckboxSelectionPropagation: true
    });
});

glossaryApp.filter('conceptFilterAndOrder', function() {
    return function(
		concepts,
		filter) {
		var results = [];
		if(!concepts) {
			return results;
		}
		//console.log("Filtering triggered");
		for (var i = 0; i < concepts.length; i++) {
			concepts[i].searchPhraseMatchScore = getMatchScore(concepts[i],filter.searchPhrase);
			if(concepts[i].searchPhraseMatchScore>0) {
				results.push(concepts[i]);
			}
		}
		//if there is no search phrase, lets just sort alphabetically (-1 means that c1 should be sorted before c2)
		if(!filter.searchPhrase) {
            results.sort(function (c1, c2) {
                var c1HasConceptLabel = conceptHasLabel(c1, "concept");
                var c2HasConceptLabel = conceptHasLabel(c2, "concept");
                var resultBasedOnLabels = c2HasConceptLabel - c1HasConceptLabel;
                if (resultBasedOnLabels === 0)
                    return getEnglishPreferredTerm(c1).localeCompare(getEnglishPreferredTerm(c2))
                else
                    return resultBasedOnLabels;
				});
		// else sort based on how well the concepts scored
		} else {
			results.sort(function(c1,c2){
							return c1.searchPhraseMatchScore-c2.searchPhraseMatchScore
							});			
		}
		return results;
		//return concepts;
    };
});

function getEnglishPreferredTerm(concept) {
    return concept._name;
    //return concept.name.__text;
};


function contains(wholeString,subString) {
	var result = //javascript doesnt allow line break for return statements
		wholeString && // checks that not undefined, null and empty
		subString && // checks that not undefined, null and empty
		wholeString.toLowerCase().indexOf(subString.toLowerCase()) > -1;
	return result;
};

/*Returns a score on how well the concept matches the search phrase.
The value is a double between [1,inf] where 1 is the best match. */
function getMatchScore(concept,searchPhrase) {
	// if the search phrase is shorter than X characters any concept will be considered a match
	if(!searchPhrase /*|| searchPhrase.length < 3*/ ) {
		return 1;
    }
    var name = getEnglishPreferredTerm(concept);
    if (
            name && //protect for null/undefined
            contains(name,searchPhrase)
		) {
         return 1.0 + (1.0 - 1.0 / name.length) - 0.1*conceptHasLabel(concept, "concept");
    }
    if (
        concept.customAttributes != null &&
        concept.customAttributes.customAttributeValue != null &&
        concept.customAttributes.customAttributeValue.attributeValue != null &&
        concept.customAttributes.customAttributeValue.attributeValue._value
    )
    var id = concept.customAttributes.customAttributeValue.attributeValue._value;
    if (
        id && //protect for null/undefined
        contains(id, searchPhrase)
    ) {
        return 1.1 + (1.0 - 1.0 / id.length) - 0.1 * conceptHasLabel(concept, "concept");
    }
    var description = concept._longDescription;
	if (
			description &&
			contains(description,searchPhrase)
		) {
            return 3.0 + (1.0 - 1.0 / description.length) - 0.5*conceptHasLabel(concept, "concept");
	}
};

// returns 1 if true, else 0
function conceptHasLabel(concept, label) {
    if (concept.labels != null && concept.labels.label != null) {
        for (var i = 0; i < concept.labels.label.length; i++) {
            if (concept.labels.label[i]._name === label)
                return 1;
        }
    }
    return 0;
};